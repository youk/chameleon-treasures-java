package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by youkun on 2018/2/23.
 * 商品信息
 */
@Entity
@Data
public class ProductInfo implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String title;   //标题

    private String description; //描述

    private String generalPrice;    //普通价格

    private String vipPrice;    //vip价格

    private int packageNumber;  //包邮件数

    private String productColor;   //颜色

    private String productSize;     //尺寸

    private String productCategory;    //商品分类

    private String productNum;  //货号

    private String fileNumber;  //档口号

    private Date startTime; //创建时间
}

