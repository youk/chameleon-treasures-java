package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by youkun on 2018/2/23.
 * 关注
 */
@Entity
@Data
public class Attention implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private Long shopId;    //关注店铺id

    private Date startTime; //关注开始时间

    private String userCode; //关注用户标识
}
