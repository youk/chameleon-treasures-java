package com.nahuobao.entity;

import com.nahuobao.entity.ext.Page;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/2/23.
 * 订单状态
 */
@Entity
@Data
public class OrderCategory extends Page implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String OrderCategoryName; //订单状态名称

    private String OrderCategoryCode;  //订单状态的标识
}
