package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.print.DocFlavor;
import java.util.Date;

/**
        * Created by youkun on 2018/2/23.
        * 订单总表
        */
@Entity
@Data
public class OrderProductPay {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private Long paymentRecordsId; //支付记录id

    private Double money;//订单金额

    private String payCategoryCode;   //支付方式

    private Long couponsId;         //优惠卷标识

    private String couponsMoney;   //优惠券金额

    private Date createTime;//创建时间

    private Date modifyTime;//修改时间

    private Date payTime;//支付时间

    private String payStatus;//支付状态

    private String userCode;    //用户标识

}
