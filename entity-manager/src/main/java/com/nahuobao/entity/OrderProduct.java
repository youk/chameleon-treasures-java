package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/2/23.
 * 订单
 */
@Entity
@Data
public class OrderProduct implements Serializable{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String orderNumber; //订单号

    private Long shopId;    //商品Id

    private String shopMoney;   //商品原价

    private String shopPayMoney; //商品支付价格

    private String shopCode;    //商品商家标识

    private String userCode;    //用户标识

    private String orderCategory;   //订单状态

    private  String orderProductPayId;//商品支付单号

    private String num; //产品数量

    private String notes;   //备注
}
