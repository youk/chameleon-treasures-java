package com.nahuobao.entity;

import com.nahuobao.entity.ext.Page;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by youkun on 2018/2/23.
 * 轮播图
 */
@Entity
@Data
public class Carousel extends Page implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String description; //描述

    private String imagePath;   //图片地址

    private String channel;     //渠道 app weChat

}
