package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/3/3.
 * 市
 */
@Entity
@Data
public class HatCity implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String cityId;      //市的id

    private String city;        //市

    private String father;      //省的id
}
