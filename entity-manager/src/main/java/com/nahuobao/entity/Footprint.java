package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by youkun on 2018/2/23.
 * 足迹
 */
@Entity
@Data
public class Footprint implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private Date startTime;     //浏览的时间

    private Long shopId;    //浏览的商家

    private Long commodityId; // 浏览的商品

    private String userCode;    //用户标识

    private String userType; //用户类型
}
