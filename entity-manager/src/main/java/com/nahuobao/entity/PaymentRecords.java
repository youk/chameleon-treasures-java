package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by youkun on 2018/2/23.
 * 支付记录
 */
@Entity
@Data
public class PaymentRecords implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String payForm;         //支付方

    private String payReceive;      //接受方

    private Date payTime;           //交易时间

    private Long operatorCode;      //操作人标识

    private boolean status;         //操作状态

    private String money;           //操作金额

    private String payEvent;        //支付的事件（干什么用的）

    private String payCategoryCode;   //支付方式

    private String orderNumber;     //订单号

    private Long couponsId;         //优惠卷标识

    private String couponsMoney;   //优惠券金额
}
