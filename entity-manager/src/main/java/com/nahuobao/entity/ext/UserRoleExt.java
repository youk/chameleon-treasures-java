package com.nahuobao.entity.ext;

import com.nahuobao.entity.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by youkun on 2018/3/14.
 * 用户角色关联
 */
@Data
public class UserRoleExt extends User implements Serializable{

    private String roleName;    //角色名称

    private String roleId;  //角色id

}
