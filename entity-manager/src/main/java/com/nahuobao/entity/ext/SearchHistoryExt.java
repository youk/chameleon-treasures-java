package com.nahuobao.entity.ext;

import com.nahuobao.entity.SearchHistoryByUser;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by youkun on 2018/3/7.
 */
@Data
public class SearchHistoryExt extends SearchHistoryByUser implements Serializable{

    private  String title;      //搜索标题

    private Long total;         //搜索的次数 用于热搜
}
