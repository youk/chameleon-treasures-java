package com.nahuobao.entity.ext;

import lombok.Data;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * Created by youkun on 2018/3/5.
 */
@Data
public class Page implements Serializable{

    @Transient
    private int pageIndex;  //当前页数

    @Transient
    private int pageSize;   //每页的条数
}
