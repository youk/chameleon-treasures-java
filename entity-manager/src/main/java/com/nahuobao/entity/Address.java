package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/2/23.
 * 地址
 */
@Entity
@Data
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String province;   //省

    private String city;        //市

    private String area;        //区

    private String address;     //详细地址

    private String userName;    //用户姓名

    private String mobile;      //用户手机号

    private String userCode;         //所属用户

    private boolean isDefault;  //是否市默认地址
}
