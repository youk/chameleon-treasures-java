package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by youkun on 2018/2/23.
 * 商品的扩展信息如：图片 视频等
 */
@Entity
@Data
public class ProductExt {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private Long productId;  //商品id

    private String productType; //商品类型 比如 图片 视频

    private String imagePath;   //图片地址

    private boolean status;     //是否是主图片


}
