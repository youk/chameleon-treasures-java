package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by youkun on 2018/2/23.
 * 评价
 */
@Entity
@Data
public class Evaluation implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String userFormCode;        //评价的用户(发起方)

    private Long shopId;            //评价的商品

    private String orderNumber;     //评价的订单号

    private String userToCode;        //评价指定的用户（接受方）

    private String shopMessage;         //商品评价的信息

    private String shopDescription;    //商品描述的评价

    private String shopLogistics;       //商品物流评价

    private String shopService;        //商家服务态度

    private Date  startTime;        //评论的时间
}
