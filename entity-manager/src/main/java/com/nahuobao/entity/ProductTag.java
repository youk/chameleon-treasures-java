package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/3/14.
 * 产品标签类似筛选中的其他 限时抢购
 */
@Entity
@Data
public class ProductTag implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String tagName;
}
