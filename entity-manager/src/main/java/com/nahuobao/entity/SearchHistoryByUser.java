package com.nahuobao.entity;

import com.nahuobao.entity.ext.Page;
import lombok.Data;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by youkun on 2018/3/7.
 * 搜索历史和用户的关系
 */
@Entity
@Data
public class SearchHistoryByUser extends Page implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private Date enDate;    //最后一次搜索的时间

    private String userCode;

    private String userType;

    private Long searchHisId;   //搜索历史的id
}
