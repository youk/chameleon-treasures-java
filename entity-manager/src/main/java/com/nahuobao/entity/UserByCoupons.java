package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/2/23.
 * 优惠卷和用户的关联关系
 */
@Entity
@Data
public class UserByCoupons implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private Long couponsId; //优惠券的Id

    private String status; //优惠券状态

    private String userId;  //用户id
}
