package com.nahuobao.entity;

import com.nahuobao.entity.ext.Page;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/1/31.
 * 用户
 */
@Data
@Entity
public class User extends Page implements Serializable {

        @Id
        @GeneratedValue(strategy= GenerationType.AUTO)
        private Long id;

        private  String userName;   //用户名

        private  String userCode; //用户标识

        private  String password;   //用户密码

        private  String iocImage;    //用户头像

        private  Double money;//账号余额

}
