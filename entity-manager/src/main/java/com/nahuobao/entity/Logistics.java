package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/2/23.
 * 物流信息
 */
@Entity
@Data
public class Logistics implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String orderNumber; //订单编号

    private Long shopId;   //商品的标识

    private String logisticsCompanyName; //物流公司名称

    private String logisticsNumber; //物流单号

    private String province;     //省

    private String city;        //市

    private String area;        //区

    private String address;     //详细地址
}
