package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by youkun on 2018/2/23.
 * 优惠券
 */
@Entity
@Data
public class Coupons implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String couponsNumber;   //优惠券编号

    private String couponsTile; //优惠券标题

    private String couponsDescription; //优惠券描述

    private Date startTime; //开始时间

    private Date endTime;   //结束时间

    private  String couponsMoney;  //优惠金额

    private String  meetMoney;  //满足多少可以使用

    private String total;   //优惠券总数量

    private String remainderTotal; //优惠券剩余的总数量
}
