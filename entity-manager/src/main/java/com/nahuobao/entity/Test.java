package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/1/20.
 */
@Data
@Entity
public class Test implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String account;

    private String phone;
    private String nickName;
    private String password;
    private String salt;
    private int userType;
    private String createUser;
    private String createTime;
    private int state;
}
