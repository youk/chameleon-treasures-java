package com.nahuobao.entity;

import com.nahuobao.entity.ext.Page;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/2/23.
 * 支付方式
 */
@Entity
@Data
public class PayCategory extends Page implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String payCategoryName; //支付方式名称

    private String payCategoryCode; //支付方式的标识
}
