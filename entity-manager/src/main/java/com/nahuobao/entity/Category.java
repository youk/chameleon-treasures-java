package com.nahuobao.entity;

import com.nahuobao.entity.ext.Page;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by youkun on 2018/2/23.
 * 类别
 */
@Entity
@Data
public class Category extends Page implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String categoryName; //类别名称

    private String categoryImage; //类别的头像

    private Long parentId;   //上级id

    @Transient
    private String parentName; //上级名称

    @Transient
    private List<Category> children;  //下级
}
