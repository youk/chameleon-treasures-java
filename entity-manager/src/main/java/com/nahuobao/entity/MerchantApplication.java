package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/2/23.
 * 商家申请开店信息
 */
@Entity
@Data
public class MerchantApplication implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String shopAvatar;  //店铺头像

    private String shopSignHead;//店铺招牌头像

    private String shopName;    //店铺名称

    private String identityPhoto; //身份证照 多个图片逗号分割

    private String contactName; //联系人姓名

    private String contactPhone;   //联系人电话

    private String province;     //省

    private String city;        //市

    private String area;        //区

    private String geographicAreaId; //批发市场名称（地理区域搜索用的）wholesaleMarketName

    private String theFloor;    //所在楼层

    private String fileNumber;  //档口编号

    private Long categoryId; //主营类别

    private String userCode;    //用户标识

}
