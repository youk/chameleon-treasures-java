package com.nahuobao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by youkun on 2018/3/3.
 * 省
 */
@Entity
@Data
public class HatProvince implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String provinceId;  //省id

    private String province;    //省

}
