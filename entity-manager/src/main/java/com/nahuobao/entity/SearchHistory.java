package com.nahuobao.entity;

import com.nahuobao.entity.ext.Page;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by youkun on 2018/2/23.
 * 搜索历史
 */
@Entity
@Data
public class SearchHistory extends Page implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private  String title;      //搜索标题

    private Long total;         //搜索的次数 用于热搜
}
