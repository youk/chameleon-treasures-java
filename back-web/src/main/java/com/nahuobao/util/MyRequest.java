package com.nahuobao.util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUpgradeHandler;
import javax.servlet.http.Part;

import com.alibaba.fastjson.JSON;
import com.tmsps.util.desutil.DesUtil;
import org.springframework.util.StringUtils;

//import com.poly.rbl.utils.FastJsonUtils;
//import com.poly.rbl.utils.RsaEncryptUtil;

/**
 * Created by youkun on 2018/2/24.
 * 自定义HttpServletRequest，统一处理请求数据解密问题
 *
 */

public class MyRequest implements HttpServletRequest
{

    public HttpServletRequest originalRequest;

    public Map decryptParameterMap;

    public MyRequest(HttpServletRequest request)
    {

        originalRequest = request;

        System.out.println(originalRequest.getRemoteAddr() + "---URL:"
                + originalRequest.getRequestURL() + "---URI:"
                + originalRequest.getRequestURI());

        // 根据你自己的需要处理逻辑

        // 不解密
        // decryptParameterMap = new HashMap();
        // try
        // {
        // request.setCharacterEncoding("UTF-8");
        // }
        // catch (UnsupportedEncodingException e)
        // {
        // e.printStackTrace();
        // }
        //
        // Map<String, String[]> properties = request.getParameterMap();
        // Map<String, String> returnMap = new HashMap<String, String>();
        // Iterator<Entry<String, String[]>> entries = properties.entrySet().iterator();
        // Entry<String, String[]> entry;
        // String key = "";
        // String value = "";
        // while (entries.hasNext())
        // {
        // entry = (Entry<String, String[]>)entries.next();
        // key = (String)entry.getKey();
        // Object valueObj = entry.getValue();
        // if (null == valueObj)
        // {
        // value = "";
        // }
        // else if (valueObj instanceof String[])
        // {
        // String[] values = (String[])valueObj;
        // for (int i = 0; i < values.length; i++ )
        // {
        // value = values[i] + ",";
        // }
        // value = value.substring(0, value.length() - 1);
        // }
        // else
        // {
        // value = valueObj.toString();
        // }
        // returnMap.put(key, value);
        // }
        //
        // decryptParameterMap.putAll(returnMap);

        String data = request.getParameter("data");

        if (!StringUtils.isEmpty(data))
        {
            try
            {
                //解密
                 System.out.println(data);
                 String deStr = DesUtil.decrypt(data,"aaaaaaaa");
                 decryptParameterMap = JSON.parseObject(deStr,Map.class);
                // 通过密钥解密参数,并转成Map
               // String deStr = RsaEncryptUtil.decryptByPrivateKey(data);
               // decryptParameterMap = FastJsonUtils.toBean(deStr, Map.class);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Object getAttribute(String s)
    {

        return originalRequest.getAttribute(s);

    }

    @Override
    public Enumeration getAttributeNames()
    {

        return originalRequest.getAttributeNames();

    }

    @Override
    public String getCharacterEncoding()
    {
        return originalRequest.getCharacterEncoding();
    }

    @Override
    public void setCharacterEncoding(String s)
            throws UnsupportedEncodingException
    {
        originalRequest.setCharacterEncoding(s);
    }

    @Override
    public int getContentLength()
    {
        return originalRequest.getContentLength();
    }

    @Override
    public long getContentLengthLong()
    {
        return originalRequest.getContentLengthLong();
    }

    @Override
    public String getContentType()
    {
        return originalRequest.getContentType();
    }

    @Override
    public ServletInputStream getInputStream()
            throws IOException
    {
        return originalRequest.getInputStream();
    }

    @Override
    public String getParameter(String s)
    {

        // 返回解密后的参数
        return String.valueOf(decryptParameterMap.get(s));
    }

    @Override
    public Enumeration getParameterNames()
    {
        // 这里是通过实体类注入参数
        return Collections.enumeration(decryptParameterMap.keySet());
    }

    @Override
    public String[] getParameterValues(String s)
    {

        // 这里是注入参数
        Object o = decryptParameterMap.get(s);
        if (o == null)
        {
            return null;
        }
        else
        {
            return new String[] {String.valueOf(o)};
        }

    }

    @Override
    public Map getParameterMap()
    {
        return originalRequest.getParameterMap();
    }

    @Override
    public String getProtocol()
    {
        return originalRequest.getProtocol();
    }

    @Override
    public String getScheme()
    {
        // TODO Auto-generated method stub
        return originalRequest.getScheme();
    }

    @Override
    public String getServerName()
    {
        // TODO Auto-generated method stub
        return originalRequest.getServerName();
    }

    @Override
    public int getServerPort()
    {
        // TODO Auto-generated method stub
        return originalRequest.getServerPort();
    }

    @Override
    public BufferedReader getReader()
            throws IOException
    {
        // TODO Auto-generated method stub
        return originalRequest.getReader();
    }

    @Override
    public String getRemoteAddr()
    {
        // TODO Auto-generated method stub
        return originalRequest.getRemoteAddr();
    }

    @Override
    public String getRemoteHost()
    {

        // TODO Auto-generated method stub
        return originalRequest.getRemoteHost();

    }

    @Override
    public void setAttribute(String s, Object obj)
    {
        originalRequest.setAttribute(s, obj);
    }

    @Override
    public void removeAttribute(String s)
    {
        // TODO Auto-generated method stub
        originalRequest.removeAttribute(s);
    }

    @Override
    public Locale getLocale()
    {

        // TODO Auto-generated method stub
        return originalRequest.getLocale();

    }

    @Override
    public Enumeration<Locale> getLocales()
    {

        // TODO Auto-generated method stub
        return originalRequest.getLocales();

    }

    @Override
    public boolean isSecure()
    {

        // TODO Auto-generated method stub
        return originalRequest.isSecure();

    }

    @Override
    public RequestDispatcher getRequestDispatcher(String s)
    {

        // TODO Auto-generated method stub

        return originalRequest.getRequestDispatcher(s);

    }

    @Override
    public String getRealPath(String s)
    {

        // TODO Auto-generated method stub
        return originalRequest.getRealPath(s);

    }

    @Override
    public int getRemotePort()
    {

        // TODO Auto-generated method stub
        return originalRequest.getRemotePort();

    }

    @Override
    public String getLocalName()
    {

        // TODO Auto-generated method stub
        return originalRequest.getLocalName();

    }

    @Override
    public String getLocalAddr()
    {

        // TODO Auto-generated method stub
        return originalRequest.getLocalAddr();

    }

    @Override
    public int getLocalPort()
    {

        // TODO Auto-generated method stub
        return originalRequest.getLocalPort();

    }

    @Override
    public ServletContext getServletContext()
    {

        // TODO Auto-generated method stub
        return originalRequest.getServletContext();

    }

    @Override
    public AsyncContext startAsync()
            throws IllegalStateException
    {

        // TODO Auto-generated method stub
        return originalRequest.startAsync();

    }

    @Override
    public AsyncContext startAsync(ServletRequest servletrequest, ServletResponse servletresponse)
            throws IllegalStateException
    {
        return originalRequest.startAsync(servletrequest, servletresponse);
    }

    @Override
    public boolean isAsyncStarted()
    {

        return originalRequest.isAsyncStarted();
    }

    @Override
    public boolean isAsyncSupported()
    {

        return originalRequest.isAsyncSupported();

    }

    @Override
    public AsyncContext getAsyncContext()
    {

        // TODO Auto-generated method stub
        return originalRequest.getAsyncContext();

    }

    @Override
    public DispatcherType getDispatcherType()
    {

        // TODO Auto-generated method stub
        return originalRequest.getDispatcherType();

    }

    @Override
    public boolean authenticate(HttpServletResponse httpservletresponse)
            throws IOException, ServletException
    {

        // TODO Auto-generated method stub
        return originalRequest.authenticate(httpservletresponse);

    }

    @Override
    public String changeSessionId()
    {

        // TODO Auto-generated method stub
        return originalRequest.changeSessionId();

    }

    @Override
    public String getAuthType()
    {

        // TODO Auto-generated method stub
        return originalRequest.getAuthType();

    }

    @Override
    public String getContextPath()
    {

        // TODO Auto-generated method stub
        return originalRequest.getContextPath();

    }

    @Override
    public Cookie[] getCookies()
    {

        // TODO Auto-generated method stub
        return originalRequest.getCookies();

    }

    @Override
    public long getDateHeader(String s)
    {

        // TODO Auto-generated method stub
        return originalRequest.getDateHeader(s);

    }

    @Override
    public String getHeader(String s)
    {

        // TODO Auto-generated method stub
        return originalRequest.getHeader(s);

    }

    @Override
    public Enumeration getHeaderNames()
    {

        // TODO Auto-generated method stub
        return originalRequest.getHeaderNames();

    }

    @Override
    public Enumeration getHeaders(String s)
    {

        // TODO Auto-generated method stub
        return originalRequest.getHeaders(s);

    }

    @Override
    public int getIntHeader(String s)
    {

        // TODO Auto-generated method stub
        return originalRequest.getIntHeader(s);

    }

    @Override
    public String getMethod()
    {

        return originalRequest.getMethod();
    }

    @Override
    public Part getPart(String s)
            throws IOException, ServletException
    {

        // TODO Auto-generated method stub
        return originalRequest.getPart(s);

    }

    @Override
    public Collection<Part> getParts()
            throws IOException, ServletException
    {

        // TODO Auto-generated method stub
        return originalRequest.getParts();

    }

    @Override
    public String getPathInfo()
    {

        // TODO Auto-generated method stub
        return originalRequest.getPathInfo();

    }

    @Override
    public String getPathTranslated()
    {

        // TODO Auto-generated method stub
        return originalRequest.getPathTranslated();

    }

    @Override
    public String getQueryString()
    {

        // TODO Auto-generated method stub
        return originalRequest.getQueryString();

    }

    @Override
    public String getRemoteUser()
    {

        // TODO Auto-generated method stub
        return originalRequest.getRemoteUser();

    }

    @Override
    public String getRequestURI()
    {

        // TODO Auto-generated method stub
        return originalRequest.getRequestURI();

    }

    @Override
    public StringBuffer getRequestURL()
    {

        // TODO Auto-generated method stub
        return originalRequest.getRequestURL();

    }

    @Override
    public String getRequestedSessionId()
    {

        // TODO Auto-generated method stub
        return originalRequest.getRequestedSessionId();

    }

    @Override
    public String getServletPath()
    {

        // TODO Auto-generated method stub
        return originalRequest.getServletPath();

    }

    @Override
    public HttpSession getSession()
    {

        // TODO Auto-generated method stub
        return originalRequest.getSession();

    }

    @Override
    public HttpSession getSession(boolean flag)
    {

        // TODO Auto-generated method stub
        return originalRequest.getSession(flag);

    }

    @Override
    public Principal getUserPrincipal()
    {

        // TODO Auto-generated method stub
        return originalRequest.getUserPrincipal();

    }

    @Override
    public boolean isRequestedSessionIdFromCookie()
    {

        // TODO Auto-generated method stub
        return originalRequest.isRequestedSessionIdFromCookie();

    }

    @Override
    public boolean isRequestedSessionIdFromURL()
    {

        // TODO Auto-generated method stub
        return originalRequest.isRequestedSessionIdFromURL();

    }

    @Override
    public boolean isRequestedSessionIdFromUrl()
    {

        // TODO Auto-generated method stub
        return originalRequest.isRequestedSessionIdFromUrl();

    }

    @Override
    public boolean isRequestedSessionIdValid()
    {

        // TODO Auto-generated method stub
        return originalRequest.isRequestedSessionIdValid();

    }

    @Override
    public boolean isUserInRole(String s)
    {

        // TODO Auto-generated method stub
        return originalRequest.isUserInRole(s);

    }

    @Override
    public void login(String s, String s1)
            throws ServletException
    {

        originalRequest.login(s, s1);

    }

    @Override
    public void logout()
            throws ServletException
    {
        originalRequest.logout();
    }

    @Override
    public <T extends HttpUpgradeHandler> T upgrade(Class<T> class1)
            throws IOException, ServletException
    {
        return originalRequest.upgrade(class1);
    }

}
