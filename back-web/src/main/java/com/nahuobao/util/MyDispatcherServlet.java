package com.nahuobao.util;

import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by youkun on 2018/2/24.
 */
public class MyDispatcherServlet extends DispatcherServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        super.doDispatch(new MyRequest(request), response);
    }
}
