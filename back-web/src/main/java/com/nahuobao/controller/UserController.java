package com.nahuobao.controller;

import com.nahuobao.entity.User;
import com.nahuobao.entity.ext.UserRoleExt;
import com.nahuobao.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by youkun on 2018/3/7.
 * 用户管理
 */
@RestController
@RequestMapping("user")
@Api(value = "用户管理",description = "用户管理")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "添加用户")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "user对象",name = "user",paramType = "body",dataType = "UserRoleExt"),
    })
    @RequestMapping(value = "addUser",method = RequestMethod.POST)
    public Map<String,Object> addUser(@RequestBody UserRoleExt user){

        return userService.insert(user);
    }

    @ApiOperation(value = "查询用户列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "user对象",name = "user",paramType = "body",dataType = "User"),
    })
    @RequestMapping(value = "queryUserList",method = RequestMethod.POST)
    public Map<String,Object> queryUserList(@RequestBody User user){

        return userService.queryUserList(user);
    }

    @ApiOperation(value = "查询用户角色列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "UserRoleExt对象",name = "user",paramType = "body",dataType = "UserRoleExt"),
    })
    @RequestMapping(value = "queryUserRoleList",method = RequestMethod.POST)
    public Map<String,Object> queryUserRoleList(@RequestBody UserRoleExt user){

        return userService.queryUserRoleList(user);
    }

    @ApiOperation(value = "根据id查询用户详情")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "id",name = "id",paramType = "path",dataType = "string")
    })
    @RequestMapping(value = "queryUserById/{id}",method = RequestMethod.GET)
    public Map<String,Object> queryUserById(@PathVariable String id){
        User user = new User();
        user.setId(Long.parseLong(id));
        return userService.queryByIdOrCode(user);
    }

    @ApiOperation(value = "根据userCode和类型查询用户详情")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "根据用户的类型和code",name = "user",paramType = "body",dataType = "User")
    })
    @RequestMapping(value = "queryUserByUserCode/{userCode}",method = RequestMethod.POST)
    public Map<String,Object> queryUserByUserCode(@RequestBody User user){
        return userService.queryByIdOrCode(user);
    }

    @ApiOperation(value = "删除一个用户")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "删除一个用户id或者userCode",name = "user",paramType = "body",dataType = "User")
    })
    @RequestMapping(value = "deleteUserByIdOrCode",method = RequestMethod.POST)
    public Map<String,Object> deleteUserByIdOrCode(@RequestBody User user){
        return userService.deleteByCodeOrId(user);
    }

    @ApiOperation(value = "更新一个用户")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "需要更新的数据根据id或者UserCode",name = "user",paramType = "body",dataType = "User")
    })
    @RequestMapping(value = "updateUserByIdOrCode",method = RequestMethod.POST)
    public Map<String,Object> updateUserByIdOrCode(@RequestBody User user){

        return userService.update(user);
    }
}
