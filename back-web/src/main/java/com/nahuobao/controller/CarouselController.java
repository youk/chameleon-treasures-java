package com.nahuobao.controller;

import com.nahuobao.entity.Carousel;
import com.nahuobao.service.CarouselService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/5.
 * 轮播图
 *
 */
@Api(value = "轮播图",description = "轮播图")
@RestController
public class CarouselController {

    @Autowired
    private CarouselService carouselService;

    @ApiOperation(value = "添加轮播图")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "Carousel对象 渠道 app,weChat",name = "轮播的实体",paramType = "body",dataType = "Carousel"),
    })
    @RequestMapping(value = "addCarousel",method = RequestMethod.POST)
    public Map<String,Object>  addCarousel(@RequestBody Carousel carousel){

        return carouselService.insert(carousel);
    }

    @ApiOperation(value = "查询轮播图列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "Carousel对象 渠道 app,weChat",name = "carousel",paramType = "body",dataType = "Carousel"),
    })
    @RequestMapping(value = "queryCarouselList",method = RequestMethod.POST)
    public Map<String,Object> queryCarouselList(@RequestBody Carousel carousel){

        return carouselService.queryCarouselList(carousel);
    }

    @ApiOperation(value = "查询轮播图详情")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "轮播图的id",name = "id",paramType = "path",dataType = "long")
    })
    @RequestMapping(value = "queryCarousel/{id}",method = RequestMethod.GET)
    public Map<String,Object> queryCarousel(@PathVariable String id){

        return carouselService.queryCarousel(Long.parseLong(id));
    }

    @ApiOperation(value = "删除一个轮播图")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "删除轮播图的id",name = "map",paramType = "body",dataType = "Map")
    })
    @RequestMapping(value = "deleteCarouselById",method = RequestMethod.POST)
        public Map<String,Object> deleteCarouselById(@RequestBody Map<String,Object> map){
            String id = map.get("id").toString();
            return carouselService.deleteCarouselById(Long.parseLong(id));
        }

    @ApiOperation(value = "删除多个轮播图",notes = "删除对象，传递数组ids")
    @RequestMapping(value = "deleteCarouselByIds",method = RequestMethod.POST)
    public Map<String,Object> deleteCarouselByIds(@RequestBody Map<String,Object> map){
        List<String> ids = (List)map.get("ids");
        return carouselService.deleteCarouselByIds(ids);
    }



    @ApiOperation(value = "更新一个轮播图")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "需要更新的数据",name = "carousel",paramType = "body",dataType = "Carousel")
    })
    @RequestMapping(value = "updateCarouselById",method = RequestMethod.POST)
    public Map<String,Object> updateCarouselById(@RequestBody Carousel carousel){

        return carouselService.updateCarouselById(carousel);
    }
}
