package com.nahuobao.controller;

import com.nahuobao.entity.ext.SearchHistoryExt;
import com.nahuobao.service.SearchHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by youkun on 2018/3/8.
 */
@RestController
@RequestMapping("history")
@Api(description = "历史搜索和热搜",value = "历史搜索和热搜")
public class SearchHistoryController {

    @Autowired
    private SearchHistoryService searchHistoryService;

    @ApiOperation(value = "添加历史搜索和热搜")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "SearchHistoryExt对象",name = "searchHistoryExt",paramType = "body",dataType = "SearchHistoryExt"),
    })
    @RequestMapping(value = "addSearchHistory",method = RequestMethod.POST)
    public Map<String,Object> addSearchHistory(@RequestBody SearchHistoryExt searchHistoryExt){

        return searchHistoryService.insert(searchHistoryExt);
    }

    @ApiOperation(value = "历史搜索和热搜的查询")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "SearchHistoryExt对象",name = "searchHistoryExt",paramType = "body",dataType = "SearchHistoryExt"),
    })
    @RequestMapping(value = "querySearchHistory",method = RequestMethod.POST)
    public Map<String,Object> querySearchHistory(@RequestBody SearchHistoryExt searchHistoryExt){

        return searchHistoryService.querySearchHistory(searchHistoryExt);
    }
}
