package com.nahuobao.controller;

import com.nahuobao.entity.OrderCategory;
import com.nahuobao.service.OrderCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/9.
 */
@RestController
@RequestMapping("orderCategory")
@Api(value = "订单状态管理",description = "订单状态管理")
public class OrderCategoryController {

    @Autowired
    private OrderCategoryService orderCategoryService;

    @ApiOperation(value = "添加订单状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "orderCategory对象",name = "orderCategory",paramType = "body",dataType = "OrderCategory"),
    })
    @RequestMapping(value = "add",method = RequestMethod.POST)
    public Map<String,Object> add(@RequestBody OrderCategory orderCategory){

        return orderCategoryService.insert(orderCategory);
    }

    @ApiOperation(value = "更新订单状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "orderCategory对象",name = "orderCategory",paramType = "body",dataType = "OrderCategory"),
    })
    @RequestMapping(value = "update",method = RequestMethod.POST)
    public Map<String,Object> update(@RequestBody OrderCategory orderCategory){

        return orderCategoryService.update(orderCategory);
    }

    @ApiOperation(value = "查看订单状态详情")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "orderCategory对象",name = "orderCategory",paramType = "body",dataType = "OrderCategory"),
    })
    @RequestMapping(value = "query",method = RequestMethod.POST)
    public Map<String,Object> query(@RequestBody OrderCategory orderCategory){

        return orderCategoryService.queryOrderCategory(orderCategory);
    }

    @ApiOperation(value = "查看订单状态列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "orderCategory对象",name = "orderCategory",paramType = "body",dataType = "OrderCategory"),
    })
    @RequestMapping(value = "queryList",method = RequestMethod.POST)
    public Map<String,Object> queryList(@RequestBody OrderCategory orderCategory){

        return orderCategoryService.queryOrderCategoryList(orderCategory);
    }

    @ApiOperation(value = "删除订单状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "orderCategory对象",name = "orderCategory",paramType = "body",dataType = "OrderCategory"),
    })
    @RequestMapping(value = "delete",method = RequestMethod.POST)
    public Map<String,Object> delete(@RequestBody OrderCategory orderCategory){

        return orderCategoryService.deleteOrderCategory(orderCategory);
    }

    @ApiOperation(value = "删除订单状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "ids是list<String>类型",name = "map",paramType = "body",dataType = "map"),
    })
    @RequestMapping(value = "deleteList",method = RequestMethod.POST)
    public Map<String,Object> delete(@RequestBody Map map){
        List<String> ids = new ArrayList<String>();
        if(map.get("ids")!=null){
             ids = (List)map.get("ids");
         }
        return orderCategoryService.deleteOrderCategoryList(ids);
    }
}
