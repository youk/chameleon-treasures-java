package com.nahuobao.controller;

import com.nahuobao.entity.ProductTag;
import com.nahuobao.service.ProductTagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by youkun on 2018/3/14.
 */
@Api(value = "产品标签",description = "产品标签")
@RestController
@RequestMapping("productTag")
public class ProductTagController {

    @Autowired
    private ProductTagService productTagService;

    @ApiOperation(value = "添加产品标签")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "ProductTag对象",name = "productTag",paramType = "body",dataType = "ProductTag"),
    })
    @RequestMapping(value = "add",method = RequestMethod.POST)
    public Map<String,Object> add(@RequestBody ProductTag productTag){

        return productTagService.insert(productTag);
    }


    @ApiOperation(value = "更新产品标签")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "ProductTag对象",name = "productTag",paramType = "body",dataType = "ProductTag"),
    })
    @RequestMapping(value = "update",method = RequestMethod.POST)
    public Map<String,Object> update(@RequestBody ProductTag productTag){

        return productTagService.update(productTag);
    }


    @ApiOperation(value = "查询产品标签")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "ProductTag对象",name = "productTag",paramType = "body",dataType = "ProductTag"),
    })
    @RequestMapping(value = "query",method = RequestMethod.POST)
    public Map<String,Object> query(@RequestBody ProductTag productTag){

        return productTagService.queryProductTag(productTag);
    }


    @ApiOperation(value = "查询产品标签集合")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "ProductTag对象",name = "productTag",paramType = "body",dataType = "ProductTag"),
    })
    @RequestMapping(value = "queryList",method = RequestMethod.POST)
    public Map<String,Object> queryList(@RequestBody ProductTag productTag){

        return productTagService.queryProductTagList(productTag);
    }

}
