package com.nahuobao.controller;

import com.nahuobao.entity.Address;
import com.nahuobao.service.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by youkun on 2018/3/7.
 */
@RestController
@RequestMapping("address")
@Api(value="地址管理",description="地址管理")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @ApiOperation(value = "添加地址")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "address对象",name = "address",paramType = "body",dataType = "Address"),
    })
    @RequestMapping(value = "addAddress",method = RequestMethod.POST)
    public Map<String,Object> addAddress(@RequestBody Address address){

        return addressService.insert(address);
    }

    @ApiOperation(value = "查询地址列表列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "address对象",name = "address",paramType = "body",dataType = "Address"),
    })
    @RequestMapping(value = "queryAddressList",method = RequestMethod.POST)
    public Map<String,Object> queryAddressList(@RequestBody Address address){

        return addressService.queryAddressList(address);
    }

    @ApiOperation(value = "查询地址详情")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "id",name = "id",paramType = "path",dataType = "long")
    })
    @RequestMapping(value = "queryAddressById/{id}",method = RequestMethod.GET)
    public Map<String,Object> queryAddressById(@PathVariable String id){

        return addressService.queryById(Long.parseLong(id));
    }

    @ApiOperation(value = "删除一个地址")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "删除一个地址id",name = "map",paramType = "body",dataType = "Map")
    })
    @RequestMapping(value = "deleteAddressById",method = RequestMethod.POST)
    public Map<String,Object> deleteAddressById(@RequestBody Map<String,Object> map){
            String id = map.get("id").toString();
        return addressService.deleteById(Long.parseLong(id));
    }

    @ApiOperation(value = "更新一个地址")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "需要更新的数据",name = "address",paramType = "body",dataType = "Address")
    })
    @RequestMapping(value = "updateAddressById",method = RequestMethod.POST)
    public Map<String,Object> updateAddressById(@RequestBody Address address){

        return addressService.update(address);
    }
}
