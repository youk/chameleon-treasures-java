package com.nahuobao.controller;

import com.nahuobao.entity.PayCategory;
import com.nahuobao.service.PayCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/9.
 */
@RestController
@RequestMapping("payCategory")
@Api(value = "支付状态管理",description = "支付状态管理")
public class PayCategoryController {

    @Autowired
    private PayCategoryService payCategoryService;

    @ApiOperation(value = "添加支付状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "payCategory对象",name = "payCategory",paramType = "body",dataType = "PayCategory"),
    })
    @RequestMapping(value = "add",method = RequestMethod.POST)
    public Map<String,Object> addAddress(@RequestBody PayCategory payCategory){

        return payCategoryService.insert(payCategory);
    }

    @ApiOperation(value = "更新支付状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "payCategory对象",name = "payCategory",paramType = "body",dataType = "PayCategory"),
    })
    @RequestMapping(value = "update",method = RequestMethod.POST)
    public Map<String,Object> update(@RequestBody PayCategory payCategory){

        return payCategoryService.updatePayCategory(payCategory);
    }

    @ApiOperation(value = "查看支付状态详情")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "payCategory对象",name = "payCategory",paramType = "body",dataType = "PayCategory"),
    })
    @RequestMapping(value = "query",method = RequestMethod.POST)
    public Map<String,Object> query(@RequestBody PayCategory payCategory){

        return payCategoryService.queryPayCategory(payCategory);
    }

    @ApiOperation(value = "查看支付状态列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "payCategory对象",name = "payCategory",paramType = "body",dataType = "PayCategory"),
    })
    @RequestMapping(value = "queryList",method = RequestMethod.POST)
    public Map<String,Object> queryList(@RequestBody PayCategory payCategory){

        return payCategoryService.queryPayCategoryList(payCategory);
    }

    @ApiOperation(value = "删除订单状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "payCategory对象",name = "payCategory",paramType = "body",dataType = "PayCategory"),
    })
    @RequestMapping(value = "delete",method = RequestMethod.POST)
    public Map<String,Object> delete(@RequestBody PayCategory payCategory){

        return payCategoryService.deletePayCategory(payCategory);
    }

    @ApiOperation(value = "删除支付状态")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "ids是list<String>类型",name = "map",paramType = "body",dataType = "map"),
    })
    @RequestMapping(value = "deleteList",method = RequestMethod.POST)
    public Map<String,Object> delete(@RequestBody Map map){
        List<String> ids = new ArrayList<String>();
        if(map.get("ids")!=null){
            ids = (List)map.get("ids");
        }
        return payCategoryService.deletePayCategoryList(ids);
    }
}
