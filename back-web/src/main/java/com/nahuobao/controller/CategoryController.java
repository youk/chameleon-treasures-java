package com.nahuobao.controller;

import com.nahuobao.entity.Category;
import com.nahuobao.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by youkun on 2018/3/12.
 * 分类查询
 */
@RestController
@RequestMapping("category")
@Api(value = "分类管理",description = "分类管理")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @ApiOperation(value = "添加分类")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "category对象",name = "category",paramType = "body",dataType = "Category"),
    })
    @RequestMapping(value = "add",method = RequestMethod.POST)
    public Map<String,Object> AddCategory(@RequestBody Category category){

        return categoryService.insert(category);
    }

    @ApiOperation(value = "查询分类树列表列表")
    @RequestMapping(value = "queryCategoryList",method = RequestMethod.GET)
    public Map<String,Object> queryAddressList(){

        return categoryService.queryCategoryList();
    }

    @ApiOperation(value = "查询分类列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "category对象",name = "category",paramType = "body",dataType = "Category")
    })
    @RequestMapping(value = "queryList",method = RequestMethod.POST)
    public Map<String,Object> queryList(@RequestBody Category category){

        return categoryService.queryList(category);
    }

    @ApiOperation(value = "查询详情")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "category对象",name = "category",paramType = "body",dataType = "Category")
    })
    @RequestMapping(value = "queryCategory",method = RequestMethod.POST)
    public Map<String,Object> queryCategory(@RequestBody Category category){

        return categoryService.queryCategory(category);
    }


    @ApiOperation(value = "更新分类")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "category对象",name = "category",paramType = "body",dataType = "Category"),
    })
    @RequestMapping(value = "update",method = RequestMethod.POST)
    public Map<String,Object> update(@RequestBody Category category){

        return categoryService.update(category);
    }

    @ApiOperation(value = "删除分类")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "id",name = "id",paramType = "path",dataType = "string"),
    })
    @RequestMapping(value = "delete/{id}",method = RequestMethod.GET)
    public Map<String,Object> delete(@PathVariable String id){

        return categoryService.delete(Long.parseLong(id));
    }
}
