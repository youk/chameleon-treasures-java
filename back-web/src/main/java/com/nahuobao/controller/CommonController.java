package com.nahuobao.controller;

import com.nahuobao.service.CommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by youkun on 2018/3/3.
 * 公用的接口
 */
@Api(value = "公用的接口",description = "公用的接口")
@RestController
public class CommonController {

    @Autowired
    private CommonService commonService;

    @ApiOperation(value = "获取所有的省",notes = "")
    @RequestMapping(value = "/queryProvinceList",method = RequestMethod.GET)
    public Map<String,Object> getProvinceList(){

        return commonService.queryHatProvinceList();
    }

    @ApiOperation(value = "获取所省下面的市",notes = "")
    @ApiImplicitParam(value = "省的Id",name = "id",paramType = "path",dataType = "String")
    @RequestMapping(value = "/queryByProvIdList/{id}",method = RequestMethod.GET)
    public Map<String,Object> queryByProvIdList(@PathVariable String id){

        return commonService.queryByProvIdList(id);
    }

    @ApiOperation(value = "获取所市下面的区",notes = "")
    @ApiImplicitParam(value = "区的Id",name = "id",paramType = "path",dataType = "string")
    @RequestMapping(value = "/queryByCityIdList/{id}",method = RequestMethod.GET)
    public Map<String,Object> queryByCityIdList(@PathVariable String id){

        return commonService.queryByCityIdList(id);
    }

    @ApiOperation(value = "上传文件",notes = "")
    @ApiImplicitParam(value = "上传文件",name = "file",paramType = "from",dataType = "file")
    @RequestMapping(value = "fileUpload",method = RequestMethod.POST)
    public  Map<String,Object> fileUpload(@RequestParam("file") MultipartFile file){
        return commonService.fileUpload(file);
    }

    @ApiOperation(value = "删除文件",notes = "")
    @ApiImplicitParam(value = "删除文件",name = "body",dataType = "string")
    @RequestMapping(value = "deleteFile",method = RequestMethod.POST)
    public  Map<String,Object> deleteFile(@RequestBody  Map<String,Object> map){
        String path = map.get("path").toString();
        return commonService.deleteFile(path);
    }

    @ApiOperation(value = "获取上传的文件",notes = "获取上传的文件")
    @ApiImplicitParam(value = "文件",name = "file",paramType = "path",dataType = "string")
    @RequestMapping(value="getUpload/{fileNam:.+}",method = RequestMethod.GET)
    void getUpload(@PathVariable("fileNam") String fileName, HttpServletResponse response){

        commonService.getImage(fileName,response);
    }
}
