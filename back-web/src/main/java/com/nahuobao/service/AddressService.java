package com.nahuobao.service;

import com.nahuobao.entity.Address;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/7.
 * 地址管理
 */
public interface AddressService {

    Map<String,Object> insert(Address record);

    Map<String,Object>  update(Address record);

    Map<String,Object>  deleteById(Long id);

    Map<String,Object>  queryAddressList(Address address);

    Map<String,Object> queryById(Long id);
}
