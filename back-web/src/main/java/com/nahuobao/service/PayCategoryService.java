package com.nahuobao.service;

import com.nahuobao.entity.PayCategory;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/9.
 */
public interface PayCategoryService {

    Map<String,Object> insert(PayCategory record);

    Map<String,Object> queryPayCategory(PayCategory payCategory);

    Map<String,Object> updatePayCategory(PayCategory payCategory);

    Map<String,Object> deletePayCategory(PayCategory payCategory);

    Map<String,Object> queryPayCategoryList(PayCategory payCategory);

    Map<String,Object> deletePayCategoryList(List<String> ids);
}
