package com.nahuobao.service;

import com.nahuobao.entity.Category;

import java.util.Map;

/**
 * Created by youkun on 2018/3/9.
 */
public interface CategoryService {

    Map<String,Object> insert(Category record);

    Map<String,Object>  queryCategory(Category category);

    Map<String,Object> queryCategoryList();

    Map<String,Object> update(Category category);

    Map<String,Object> delete(Long id);

    Map<String,Object> queryList(Category category);

}
