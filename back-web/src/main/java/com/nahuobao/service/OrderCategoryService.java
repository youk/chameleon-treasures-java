package com.nahuobao.service;

import com.nahuobao.entity.OrderCategory;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/9.
 */
public interface OrderCategoryService {

    Map<String,Object> insert(OrderCategory orderCategory);

    Map<String,Object> update(OrderCategory orderCategory);

    Map<String,Object> queryOrderCategory(OrderCategory orderCategory);

    Map<String,Object> queryOrderCategoryList(OrderCategory orderCategory);

    Map<String,Object> deleteOrderCategory(OrderCategory orderCategory);

    Map<String,Object> deleteOrderCategoryList(List<String> ids);
}
