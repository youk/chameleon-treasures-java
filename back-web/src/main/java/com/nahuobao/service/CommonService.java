package com.nahuobao.service;

import com.nahuobao.entity.HatProvince;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/3.
 * 公用的接口
 */
public interface CommonService {

    //查询所有的省
    Map<String,Object> queryHatProvinceList();

    //查询省下面的区
    Map<String,Object> queryByProvIdList(String provId);

    //查询市下面的区
    Map<String,Object> queryByCityIdList(String cityId);

    //上传文件
    Map<String,Object> fileUpload(MultipartFile file);

    //删除文件
    Map<String,Object> deleteFile(String path);

    //获取图片
    void getImage(String fileName, HttpServletResponse response);
}
