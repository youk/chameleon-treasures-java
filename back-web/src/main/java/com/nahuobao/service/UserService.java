package com.nahuobao.service;

import com.nahuobao.entity.User;
import com.nahuobao.entity.ext.UserRoleExt;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/7.
 * 用户管理
 */
public interface UserService {

    Map<String,Object> insert(UserRoleExt record);

    Map<String,Object>  queryByIdOrCode(User user);

    Map<String,Object>  queryUserList(User user);

    Map<String,Object>  update(User user);

    Map<String,Object>  deleteByCodeOrId(User user);

    Map<String,Object> queryUserRoleList(UserRoleExt userRoleExt);
}
