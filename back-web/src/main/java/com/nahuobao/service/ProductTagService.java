package com.nahuobao.service;

import com.nahuobao.entity.ProductTag;

import java.util.Map;

/**
 * Created by youkun on 2018/3/14.
 * 产品标签
 */
public interface ProductTagService {

    Map<String,Object> queryProductTag(ProductTag productTag);

    Map<String,Object> insert(ProductTag productTag);

    Map<String,Object> update(ProductTag productTag);

    Map<String,Object> queryProductTagList(ProductTag productTag);
}
