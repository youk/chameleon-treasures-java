package com.nahuobao.service;

import com.nahuobao.entity.Carousel;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/5.
 * 轮播图
 */
public interface CarouselService {

    Map<String,Object> insert(Carousel carousel);

    Map<String,Object> queryCarousel(Long id);

    Map<String,Object> deleteCarouselById(Long id);

    Map<String,Object> updateCarouselById(Carousel carousel);

    Map<String,Object> queryCarouselList(Carousel carousel);

    Map<String,Object> deleteCarouselByIds(List<String> ids);
}
