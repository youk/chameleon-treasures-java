package com.nahuobao.service;

import com.nahuobao.entity.SearchHistory;
import com.nahuobao.entity.SearchHistoryByUser;
import com.nahuobao.entity.ext.SearchHistoryExt;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/8.
 */
public interface SearchHistoryService {

    Map<String,Object> insert(SearchHistoryExt record);

    Map<String,Object> querySearchHistory(SearchHistoryExt searchHistoryExt);
}
