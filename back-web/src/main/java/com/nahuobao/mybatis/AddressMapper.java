package com.nahuobao.mybatis;

import com.nahuobao.entity.Address;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AddressMapper {
    int insert(Address record);

    int update(Address record);

    int deleteById(Long id);

    List<Address> queryAddressList(Address address);

    Address queryById(Long id);
}