package com.nahuobao.mybatis;

import com.nahuobao.entity.PayCategory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PayCategoryMapper {
    int insert(PayCategory record);

    int insertSelective(PayCategory record);

    PayCategory queryPayCategory(PayCategory payCategory);

    int updatePayCategory(PayCategory payCategory);

    int deletePayCategory(PayCategory payCategory);

    List<PayCategory> queryPayCategoryList(PayCategory payCategory);

    int deletePayCategoryList(List<String> ids);
}