package com.nahuobao.mybatis;

import com.nahuobao.entity.SearchHistory;
import com.nahuobao.entity.SearchHistoryByUser;
import com.nahuobao.entity.ext.SearchHistoryExt;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 搜索记录 和 热搜
 */
@Mapper
public interface SearchHistoryMapper {

    int insert(SearchHistory record);

    int insertSelective(SearchHistory record);

    int insertSearchHistoryByUser(SearchHistoryByUser searchHistoryByUser);

    SearchHistory querySearchHistoryByTile(SearchHistory searchHistory);

    int updateSearchHistoryTotal(SearchHistory  searchHistory);

    List<SearchHistoryExt> querySearchHistory(SearchHistoryExt searchHistoryExt);

    int updateSearchHistoryByUserEnDate(SearchHistoryByUser searchHistoryByUser);
}