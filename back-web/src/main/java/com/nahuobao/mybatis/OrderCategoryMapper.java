package com.nahuobao.mybatis;

import com.nahuobao.entity.OrderCategory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrderCategoryMapper {
    int insert(OrderCategory record);

    int insertSelective(OrderCategory record);

    int update(OrderCategory orderCategory);

    OrderCategory queryOrderCategory(OrderCategory orderCategory);

    List<OrderCategory> queryOrderCategoryList(OrderCategory orderCategory);

    int deleteOrderCategory(OrderCategory orderCategory);

    int deleteOrderCategoryList(List<String> ids);
}