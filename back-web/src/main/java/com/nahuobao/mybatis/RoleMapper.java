package com.nahuobao.mybatis;

import com.nahuobao.entity.Role;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RoleMapper {
    int insert(Role record);

    int insertSelective(Role record);
}