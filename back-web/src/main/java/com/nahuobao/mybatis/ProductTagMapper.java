package com.nahuobao.mybatis;

import com.nahuobao.entity.ProductTag;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProductTagMapper {
    int insert(ProductTag record);

    int insertSelective(ProductTag record);

    ProductTag queryProductTag(ProductTag productTag);

    List<ProductTag> queryProductTagList(ProductTag productTag);

    int update(ProductTag productTag);
}