package com.nahuobao.mybatis;

import com.nahuobao.entity.HatProvince;
import org.apache.ibatis.annotations.Mapper;
import org.hibernate.annotations.Cache;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

@Mapper
@CacheConfig(cacheNames = "HatProvinceMapper")
public interface HatProvinceMapper {
    int insert(HatProvince record);

    int insertSelective(HatProvince record);

    //查询所有的省
    @Cacheable("queryAllList")
    List<HatProvince> queryAllList();
}