package com.nahuobao.mybatis;

import com.nahuobao.entity.HatArea;
import com.nahuobao.entity.HatCity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

@Mapper
@CacheConfig(cacheNames = "HatCityMapper")
public interface HatCityMapper {

    int insert(HatCity record);

    int insertSelective(HatCity record);

    @Cacheable(key="#p0")
    List<HatCity> queryByProvIdList(String provId);
}