package com.nahuobao.mybatis;

import com.nahuobao.entity.User;
import com.nahuobao.entity.UserByRole;
import com.nahuobao.entity.ext.UserRoleExt;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    int insert(User record);

    int insertSelective(User record);

    User queryByIdOrCode(User user);

    List<User> queryUserList(User user);

    int update(User user);

    int deleteByCodeOrId(User user);

    //查询用户的所有角色
    List<UserRoleExt> queryUserRoleList(UserRoleExt userRoleExt);

}