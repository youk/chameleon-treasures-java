package com.nahuobao.mybatis;

import com.nahuobao.entity.Category;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CategoryMapper {
    int insert(Category record);

    int insertSelective(Category record);

    Category queryCategory(Category category);

    List<Category> queryCategoryList(Category category);

    int update(Category category);

    int delete(Long id);
}