package com.nahuobao.mybatis;

import com.nahuobao.entity.UserByRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserByRoleMapper {
    int insert(UserByRole record);

    int insertSelective(UserByRole record);
}