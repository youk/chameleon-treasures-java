package com.nahuobao.mybatis;

import com.nahuobao.entity.HatArea;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

@Mapper
@CacheConfig(cacheNames = "HatAreaMapper")
public interface HatAreaMapper {
    int insert(HatArea record);

    int insertSelective(HatArea record);

    @Cacheable(key="#p0")
    List<HatArea> queryByCityIdList(String cityId);
}