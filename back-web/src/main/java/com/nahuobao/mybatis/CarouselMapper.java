package com.nahuobao.mybatis;

import com.nahuobao.entity.Carousel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

@Mapper
@CacheConfig(cacheNames = "CarouselMapper")
public interface CarouselMapper {

    int insert(Carousel record);

    int insertSelective(Carousel record);

    //添加到缓存中
    @Cacheable(key = "#p0.toString()")
    Carousel queryCarousel(Long id);

    //添加到缓存中
    @CacheEvict(key = "#p0.toString()")
    int deleteCarouselById(Long id);

    //添加到缓存中
    @CacheEvict(key = "#p0.id.toString()")
    int updateCarouselById(Carousel carousel);

    List<Carousel> queryCarouselList(Carousel carousel);

    //清空CarouselMapper的所有缓存
    @CacheEvict(cacheNames="CarouselMapper",allEntries=true )
    int deleteCarouselByIds(List<String> ids);

    List<Carousel> queryListByIds(List<String> ids);
}