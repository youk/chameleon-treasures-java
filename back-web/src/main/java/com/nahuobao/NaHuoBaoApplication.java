package com.nahuobao;

import com.nahuobao.util.FilterConfig;
import com.nahuobao.util.MyDispatcherServlet;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.DispatcherServlet;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by youkun on 2018/1/20.
 */
@SpringBootApplication
@EnableCaching
@EnableSwagger2
@EnableTransactionManagement
public class NaHuoBaoApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(NaHuoBaoApplication.class, args);
    }

    /**
     * 过滤 7⃣
     * @return
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        FilterConfig filterConfig = new FilterConfig();
        registrationBean.setFilter(filterConfig);
        List<String> urlPatterns = new ArrayList<String>();
        urlPatterns.add("/*");
        registrationBean.setUrlPatterns(urlPatterns);
        return registrationBean;
    }

    /**
     * Description: 注册自定义的DispatcherServlet，用于解决加解密不破坏spring特征
     *
     * @return
     * @see
     */
//    @Bean
//    @Qualifier(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)
//    public DispatcherServlet dispatcherServlet()
//    {
//        return new MyDispatcherServlet();
//    }
}
