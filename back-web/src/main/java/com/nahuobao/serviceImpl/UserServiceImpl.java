package com.nahuobao.serviceImpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nahuobao.common.ErrorCode;
import com.nahuobao.common.ResultUtil;
import com.nahuobao.entity.User;
import com.nahuobao.entity.UserByRole;
import com.nahuobao.entity.ext.UserRoleExt;
import com.nahuobao.mybatis.UserByRoleMapper;
import com.nahuobao.mybatis.UserMapper;
import com.nahuobao.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/7.
 * 用户管理
 */
@Service
public class UserServiceImpl implements UserService{

    private Logger logger = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserByRoleMapper userByRoleMapper;
    /**
     *  新增
     * @param record
     * @return
     */
    @Override
    public Map<String, Object> insert(UserRoleExt record) {
        try {
            User userObject = userMapper.queryByIdOrCode(record);
            if(userObject!=null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10003.getCode(),ErrorCode.ERROR_CODE_10003.getMessage(),record.getUserCode()+"用户已存在");
            }
            userMapper.insert(record);
            //添加默认角色
            UserByRole userByRole  = new UserByRole();
            userByRole.setRoleId(record.getRoleId());
            userByRole.setUserId(record.getId().toString());
            userByRoleMapper.insert(userByRole);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }

    /**
     * 查询
     * @param user
     * @return
     */
    @Override
    public Map<String, Object> queryByIdOrCode(User user) {
        User userObject = null;
        try {
            userObject = userMapper.queryByIdOrCode(user);
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),userObject);
    }

    /**
     * 查询集合
     * @param user
     * @return
     */
    @Override
    public Map<String, Object> queryUserList(User user) {
        PageInfo<User> pgeUserList = null;
        try {
            PageHelper.startPage(user.getPageIndex(),user.getPageSize(),true);
            List<User> userList = userMapper.queryUserList(user);
            pgeUserList = new PageInfo<User>(userList);
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),pgeUserList);
    }


    /**
     * 更新
     * @param user
     * @return
     */
    @Override
    public Map<String, Object> update(User user) {
        try {
            User userObject = userMapper.queryByIdOrCode(user);
            if(userObject==null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),"没有查询到当前记录");
            }
            userMapper.update(user);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"更新成功");
    }

    /**
     * 删除
     * @param user
     * @return
     */
    @Override
    public Map<String, Object> deleteByCodeOrId(User user) {
        try {
            User userObject = userMapper.queryByIdOrCode(user);
            if(userObject==null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),"没有查询到当前记录");
            }
            userMapper.deleteByCodeOrId(user);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"删除成功");
    }

    /**
     * 查询用户拥有的角色
     * @param userRoleExt
     * @return
     */
    @Override
    public Map<String, Object> queryUserRoleList(UserRoleExt userRoleExt) {
        List<UserRoleExt> userObject = null;
        try {
            userObject = userMapper.queryUserRoleList(userRoleExt);
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),userObject);
    }
}
