package com.nahuobao.serviceImpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nahuobao.common.ErrorCode;
import com.nahuobao.common.ResultUtil;
import com.nahuobao.entity.OrderCategory;
import com.nahuobao.mybatis.OrderCategoryMapper;
import com.nahuobao.service.OrderCategoryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/9.
 */
@Service
public class OrderCategoryServiceImpl implements OrderCategoryService{

    private Logger logger = Logger.getLogger(OrderCategoryServiceImpl.class);

    @Autowired
    private OrderCategoryMapper orderCategoryMapper;

    @Override
    public Map<String, Object> insert(OrderCategory orderCategory) {
        try {
            OrderCategory query = new OrderCategory();
            query.setOrderCategoryCode(orderCategory.getOrderCategoryCode());
            OrderCategory order = orderCategoryMapper.queryOrderCategory(query);
            if(order!=null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),orderCategory.getOrderCategoryCode()+"已存在!");
            }
            orderCategoryMapper.insert(orderCategory);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }

    @Override
    public Map<String, Object> update(OrderCategory orderCategory) {
        try {
            OrderCategory query = new OrderCategory();
            query.setId(orderCategory.getId());
            OrderCategory order = orderCategoryMapper.queryOrderCategory(query);
            if(order==null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),"没有查询到当前记录");
            }
            if(!order.getOrderCategoryCode().equals(orderCategory.getOrderCategoryCode())){
                OrderCategory queryOne = new OrderCategory();
                queryOne.setOrderCategoryCode(orderCategory.getOrderCategoryCode());
                OrderCategory orderOne = orderCategoryMapper.queryOrderCategory(queryOne);
                if(orderOne!=null){
                    return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),orderCategory.getOrderCategoryCode()+"已存在!");
                }
            }
            orderCategoryMapper.update(orderCategory);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"更新成功");
    }

    @Override
    public Map<String, Object> queryOrderCategory(OrderCategory orderCategory) {
        OrderCategory queryOrderCategory = new OrderCategory();
        try {
            queryOrderCategory = orderCategoryMapper.queryOrderCategory(orderCategory);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),queryOrderCategory);
    }

    @Override
    public Map<String, Object> queryOrderCategoryList(OrderCategory orderCategory) {
        PageInfo<OrderCategory> list = null;
        try {
            PageHelper.startPage(orderCategory.getPageIndex(),orderCategory.getPageSize(),true);
            List<OrderCategory> queryList = orderCategoryMapper.queryOrderCategoryList(orderCategory);
            list = new PageInfo<OrderCategory>(queryList);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),list);
    }

    @Override
    public Map<String, Object> deleteOrderCategory(OrderCategory orderCategory) {
        try {
            orderCategoryMapper.deleteOrderCategory(orderCategory);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }


    @Override
    public Map<String, Object> deleteOrderCategoryList(List<String> ids) {
        try {
            orderCategoryMapper.deleteOrderCategoryList(ids);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }
}
