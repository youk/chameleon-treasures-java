package com.nahuobao.serviceImpl;

import com.nahuobao.common.ErrorCode;
import com.nahuobao.common.ResultUtil;
import com.nahuobao.entity.Address;
import com.nahuobao.mybatis.AddressMapper;
import com.nahuobao.service.AddressService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/7.
 */
@Service
public class AddressServiceImpl  implements AddressService{

    private Logger logger = Logger.getLogger(AddressServiceImpl.class);

    @Autowired
    private AddressMapper addressMapper;

    /**
     * 新增
     * @param record
     * @return
     */
    @Override
    public Map<String, Object> insert(Address record) {
        try {
            addressMapper.insert(record);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }

    /**
     * 更新
     * @param record
     * @return
     */
    @Override
    public Map<String, Object> update(Address record) {
        try {
            Address address = addressMapper.queryById(record.getId());
            if(address==null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),"没有查询到当前记录");
            }
            addressMapper.update(record);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"更新成功");
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> deleteById(Long id) {
        try {
            Address carousel = addressMapper.queryById(id);
            if(carousel==null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),"没有查询到当前记录");
            }
            addressMapper.deleteById(id);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"删除成功");
    }

    /**
     * 查询列表
     * @param address
     * @return
     */
    @Override
    public Map<String, Object> queryAddressList(Address address) {
        List<Address> list = null;
        try {
             list = addressMapper.queryAddressList(address);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),list);
    }

    /***
     * 查询详情
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> queryById(Long id) {
        Address address = new Address();
        try {
            address = addressMapper.queryById(id);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),address);
    }
}
