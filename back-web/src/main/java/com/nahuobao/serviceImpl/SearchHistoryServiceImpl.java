package com.nahuobao.serviceImpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nahuobao.common.ErrorCode;
import com.nahuobao.common.ResultUtil;
import com.nahuobao.entity.SearchHistory;
import com.nahuobao.entity.SearchHistoryByUser;
import com.nahuobao.entity.ext.SearchHistoryExt;
import com.nahuobao.mybatis.SearchHistoryMapper;
import com.nahuobao.service.SearchHistoryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/8.
 * 历史记录 热搜 查询
 */
@Service
public class SearchHistoryServiceImpl implements SearchHistoryService{

    private Logger logger = Logger.getLogger(AddressServiceImpl.class);

    @Autowired
    private SearchHistoryMapper searchHistoryMapper;

    /**
     * 添加搜索记录
     * @param record
     * @return
     */
    @Override
    @Transactional
    public Map<String, Object> insert(SearchHistoryExt record) {
        try {
            //搜索产品 后面添加

            //添加历史表里面
            addInsert(record);
        }catch (Exception e){
            logger.error(e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }


    /***
     * 根据标题模糊匹配
     * @param searchHistoryExt
     * @return
     */
    @Override
    public Map<String, Object> querySearchHistory(SearchHistoryExt searchHistoryExt) {
        PageInfo<SearchHistoryExt> list = null;
        try {
            PageHelper.startPage(searchHistoryExt.getPageIndex(),searchHistoryExt.getPageSize(),true);
            List<SearchHistoryExt> queryList = searchHistoryMapper.querySearchHistory(searchHistoryExt);
            list = new PageInfo<SearchHistoryExt>(queryList);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),list);
    }



    /**
     * 添加历史搜索和热搜
     * @param record
     * @return
     */
    public void addInsert(SearchHistoryExt record) throws Exception{

        try {
            /**
             * 查询搜索历史中是否有这条记录,如果有就更新总记录数
             * 没有就插入一条新的记录
             */
            SearchHistory s = new SearchHistory();
            s.setTitle(record.getTitle());
            SearchHistory searchHistory = searchHistoryMapper.querySearchHistoryByTile(s);
            if(searchHistory==null){
                SearchHistory insertS = new SearchHistory();
                insertS.setTitle(record.getTitle());
                insertS.setTotal(1L);
                int i = searchHistoryMapper.insert(insertS);
                //插入用户搜索的关系
                SearchHistoryByUser searchHistoryByUser = new SearchHistoryByUser();
                searchHistoryByUser.setUserCode(record.getUserCode());
                searchHistoryByUser.setEnDate(new Date());
                searchHistoryByUser.setSearchHisId(insertS.getId());
                searchHistoryByUser.setUserType(record.getUserType());
                searchHistoryMapper.insertSearchHistoryByUser(searchHistoryByUser);
            }else{
                /**
                 * 查询当前用户是否已经搜索过如果搜索过就更新时间
                 * 否则就插入一条新记录
                 */
                SearchHistoryExt query = new SearchHistoryExt();
                query.setSearchHisId(searchHistory.getId());
                query.setUserCode(record.getUserCode());
                query.setUserType(record.getUserType());
                List<SearchHistoryExt> querySearchHistoryByUser = searchHistoryMapper.querySearchHistory(query);
                if(querySearchHistoryByUser!=null && querySearchHistoryByUser.size()>0){
                    SearchHistoryByUser insertSearchHistoryByUser = new SearchHistoryByUser();
                    insertSearchHistoryByUser.setId(querySearchHistoryByUser.get(0).getId());
                    insertSearchHistoryByUser.setEnDate(new Date());
                    //更新时间
                    searchHistoryMapper.updateSearchHistoryByUserEnDate(insertSearchHistoryByUser);
                }else{
                    SearchHistoryByUser insertSearchHistoryByUser = new SearchHistoryByUser();
                    insertSearchHistoryByUser.setUserCode(record.getUserCode());
                    insertSearchHistoryByUser.setEnDate(new Date());
                    insertSearchHistoryByUser.setSearchHisId(searchHistory.getId());
                    insertSearchHistoryByUser.setUserType(record.getUserType());
                    searchHistoryMapper.insertSearchHistoryByUser(insertSearchHistoryByUser);
                }
                //更新记录数
                searchHistory.setTotal(searchHistory.getTotal()+1);
                searchHistoryMapper.updateSearchHistoryTotal(searchHistory);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }
}
