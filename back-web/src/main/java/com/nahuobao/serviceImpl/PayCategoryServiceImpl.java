package com.nahuobao.serviceImpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nahuobao.common.ErrorCode;
import com.nahuobao.common.ResultUtil;
import com.nahuobao.entity.PayCategory;
import com.nahuobao.mybatis.PayCategoryMapper;
import com.nahuobao.service.PayCategoryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/9.
 */
@Service
public class PayCategoryServiceImpl implements PayCategoryService{

    private Logger logger = Logger.getLogger(PayCategoryServiceImpl.class);

    @Autowired
    private PayCategoryMapper payCategoryMapper;

    @Override
    public Map<String, Object> insert(PayCategory record) {
        try {
            PayCategory query = new PayCategory();
            query.setPayCategoryCode(record.getPayCategoryCode());
            PayCategory pay = payCategoryMapper.queryPayCategory(query);
            if(pay!=null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),record.getPayCategoryCode()+"已存在!");
            }
            payCategoryMapper.insert(record);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }

    @Override
    public Map<String, Object> queryPayCategory(PayCategory payCategory) {
        PayCategory queryPayCategory = new PayCategory();
        try {
            queryPayCategory = payCategoryMapper.queryPayCategory(payCategory);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),queryPayCategory);
    }

    @Override
    public Map<String, Object> updatePayCategory(PayCategory payCategory) {
        try {
            PayCategory query = new PayCategory();
            query.setId(payCategory.getId());
            PayCategory pay = payCategoryMapper.queryPayCategory(query);
            if(pay==null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),"没有查询到当前记录");
            }
            if(!pay.getPayCategoryCode().equals(payCategory.getPayCategoryCode())){
                PayCategory queryOne = new PayCategory();
                queryOne.setPayCategoryCode(payCategory.getPayCategoryCode());
                PayCategory payOne = payCategoryMapper.queryPayCategory(queryOne);
                if(payOne!=null){
                    return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),payCategory.getPayCategoryCode()+"已存在！");
                }
            }
            payCategoryMapper.updatePayCategory(payCategory);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"更新成功");
    }

    @Override
    public Map<String, Object> deletePayCategory(PayCategory payCategory) {
        try {
            payCategoryMapper.deletePayCategory(payCategory);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }

    @Override
    public Map<String, Object> queryPayCategoryList(PayCategory payCategory) {
        PageInfo<PayCategory> list = null;
        try {
            PageHelper.startPage(payCategory.getPageIndex(),payCategory.getPageSize(),true);
            List<PayCategory> queryList = payCategoryMapper.queryPayCategoryList(payCategory);
            list = new PageInfo<PayCategory>(queryList);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),list);
    }

    @Override
    public Map<String, Object> deletePayCategoryList(List<String> ids) {
        try {
            payCategoryMapper.deletePayCategoryList(ids);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }
}
