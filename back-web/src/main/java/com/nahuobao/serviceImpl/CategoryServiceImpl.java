package com.nahuobao.serviceImpl;
import com.github.pagehelper.PageHelper;
import com.nahuobao.common.ErrorCode;
import com.nahuobao.common.ResultUtil;
import com.nahuobao.entity.Category;
import com.nahuobao.mybatis.CategoryMapper;
import com.nahuobao.service.CategoryService;
import com.nahuobao.util.PageInfo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/9.
 */
@Service
public class CategoryServiceImpl implements CategoryService{

    private Logger logger = Logger.getLogger(CategoryServiceImpl.class);

    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 添加
     * @param record
     * @return
     */
    @Override
    public Map<String, Object> insert(Category record) {
        try {
            Category query = new Category();
            query.setCategoryName(record.getCategoryName());
            Category order = categoryMapper.queryCategory(query);
            if(order!=null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),record.getCategoryName()+"已存在!");
            }
            categoryMapper.insert(record);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }

    /**
     * 查询详情
     * @param category
     * @return
     */
    @Override
    public Map<String, Object> queryCategory(Category category) {
        Category queryOrderCategory = new Category();
        try {
            queryOrderCategory = categoryMapper.queryCategory(category);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),queryOrderCategory);
    }

    /**
     * 查询所有的分类
     * @return
     */
    @Override
    public Map<String, Object> queryCategoryList() {
        List<Category> resultList = new ArrayList<Category>();
        try {
            List<Category> queryOrderCategory = categoryMapper.queryCategoryList(null);
            resultList = getChildren(queryOrderCategory,0L);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),resultList);
    }

    /**
     * 更新
     * @param category
     * @return
     */
    @Override
    public Map<String, Object> update(Category category) {
        try {
            Category query = new Category();
            query.setId(category.getId());
            Category order = categoryMapper.queryCategory(query);
            if(order==null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),"没有查询到当前记录");
            }
            if(!order.getCategoryName().equals(category.getCategoryName())){
                Category queryOne = new Category();
                queryOne.setCategoryName(category.getCategoryName());
                Category orderOne = categoryMapper.queryCategory(queryOne);
                if(orderOne!=null){
                    return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),category.getCategoryName()+"已存在!");
                }
            }
            categoryMapper.update(category);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"更新成功");
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    @Transactional
    public Map<String, Object> delete(Long id) {
        try {
            categoryMapper.delete(id);
            deleteList(id);
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }

    /**
     * 查询分类的列表
     * @param category
     * @return
     */
    @Override
    public Map<String, Object> queryList(Category category) {
        PageInfo<Category> list = null;
        try {
            PageHelper.startPage(category.getPageIndex(),category.getPageSize(),true);
            List<Category> resultList = categoryMapper.queryCategoryList(category);
            list = new PageInfo<Category>(resultList);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),list);
    }

    /**
     * 删除所有的下级
     * @param id
     * @throws Exception
     */
    public  void deleteList(Long id) throws Exception{
        List<Category> list = null;
        try {
            Category c = new Category();
            c.setParentId(id);
            list = categoryMapper.queryCategoryList(c);
            if(list!=null && list.size()>0){
                for (Category s : list){
                    categoryMapper.delete(s.getId());
                    deleteList(s.getId());
                }
            }
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    public List<Category> getChildren(List<Category> list,Long parentId){
        List<Category> resultList = new ArrayList<Category>();
        for(Category c : list){
            if(parentId==c.getParentId()){
                Category cc = new Category();
                cc = c;
                List<Category> childrenList = getChildren(list,c.getId());
                cc.setChildren(childrenList);
                resultList.add(cc);
            }
        }
        return resultList;
    }
}
