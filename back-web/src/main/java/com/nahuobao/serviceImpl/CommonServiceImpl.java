package com.nahuobao.serviceImpl;

import com.nahuobao.common.FileUploadUtil;
import com.nahuobao.common.ResultUtil;
import com.nahuobao.entity.HatArea;
import com.nahuobao.entity.HatCity;
import com.nahuobao.entity.HatProvince;
import com.nahuobao.common.ErrorCode;
import com.nahuobao.mybatis.HatAreaMapper;
import com.nahuobao.mybatis.HatCityMapper;
import com.nahuobao.mybatis.HatProvinceMapper;
import com.nahuobao.service.CommonService;
import io.swagger.annotations.ApiImplicitParam;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/3.
 */
@Service
public class CommonServiceImpl implements CommonService{

    private Logger logger = Logger.getLogger(CommonServiceImpl.class);

    @Autowired
    private HatProvinceMapper hatProvinceMapper;

    @Autowired
    private HatAreaMapper hatAreaMapper;

    @Autowired
    private HatCityMapper hatCityMapper;

    @Autowired
    private Environment environment;

    /**
     * 查询所有的省
     * @return
     */
    @Override
    public Map<String, Object> queryHatProvinceList() {
        List<HatProvince> hatProvinceList =null;
        try{
            hatProvinceList = hatProvinceMapper.queryAllList();
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),e.getMessage(),null);
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),hatProvinceList);
    }

    /**
     * 查询省下面的市
     * @param provId
     * @return
     */
    @Override
    public Map<String, Object> queryByProvIdList(String provId) {
        List<HatCity> hatCityList =null;
        try{
            hatCityList = hatCityMapper.queryByProvIdList(provId);
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),e.getMessage(),null);
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),hatCityList);
    }

    /**
     * 查询市下面的区
     * @param cityId
     * @return
     */
    @Override
    public Map<String, Object> queryByCityIdList(String cityId) {
        List<HatArea> hatAreaList =null;
        try{
            hatAreaList = hatAreaMapper.queryByCityIdList(cityId);
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),e.getMessage(),null);
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),hatAreaList);
    }

    /**
     * 上传文件
     * @param file
     * @return
     */
    @Override
    public Map<String, Object> fileUpload(MultipartFile file) {
        String resultPaht = "";
        try{
            String path =environment.getProperty("carouselPath",String.class);
            resultPaht = FileUploadUtil.upload(file,path);
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),e.getMessage(),null);
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),resultPaht);
    }

    /**
     * 删除文件
     * @param path
     * @return
     */
    @Override
    public Map<String, Object> deleteFile(String path) {
        try{
            FileUploadUtil.deleteFile(environment.getProperty("carouselPath",String.class)+File.separator+path);
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),e.getMessage(),null);
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"删除文件成功");
    }


    /***
     * 获取文件
     * @param fileName
     * @param response
     */

    public void getImage(String fileName, HttpServletResponse response) {
        ServletOutputStream out = null;
        FileInputStream ips = null;
        try {
            //获取图片存放路径
            String imgPath = environment.getProperty("carouselPath",String.class)+File.separator+fileName;
            ips = new FileInputStream(new File(imgPath));
            response.setContentType("multipart/form-data");
            out = response.getOutputStream();
            //读取文件流
            int len = 0;
            byte[] buffer = new byte[1024 * 10];
            while ((len = ips.read(buffer)) != -1){
                out.write(buffer,0,len);
            }
            out.flush();
            out.close();
            ips.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
