package com.nahuobao.serviceImpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.nahuobao.common.FileUploadUtil;
import com.nahuobao.common.ResultUtil;
import com.nahuobao.entity.Carousel;
import com.nahuobao.common.ErrorCode;
import com.nahuobao.mybatis.CarouselMapper;
import com.nahuobao.service.CarouselService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/5.
 * 轮播图
 */
@Service
public class CarouselServiceImpl implements CarouselService{

    private  Logger logger = Logger.getLogger(CarouselServiceImpl.class);

    @Autowired
    private CarouselMapper carouselMapper;

    @Autowired
    private Environment environment;
    /**
     * 添加轮播图
     * @param carousel
     * @return
     */
    @Override
    public Map<String, Object> insert(Carousel carousel) {
        try {
            carouselMapper.insert(carousel);
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }

    /**
     * 查询轮播的的详情
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> queryCarousel(Long id) {
        Carousel carousel = null;
        try {
            carousel = carouselMapper.queryCarousel(id);
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),carousel);
    }

    /**
     * 删除某个轮播图
     * @param id
     * @return
     */
    @Override
    @Transactional
    public Map<String, Object> deleteCarouselById(Long id) {
        try {
           Carousel carousel = carouselMapper.queryCarousel(id);
           if(carousel==null){
               return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),"没有查询到当前记录");
           }
           //删除图片
            FileUploadUtil.deleteFile(environment.getProperty("carouselPath",String.class)+ File.separator+carousel.getImagePath());
           carouselMapper.deleteCarouselById(id);
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"删除成功");
    }

    /**
     * 更新轮播图
     * @param carousel
     * @return
     */
    @Override
    @Transactional
    public Map<String, Object> updateCarouselById(Carousel carousel) {
        try {
            Carousel carouselOne = carouselMapper.queryCarousel(carousel.getId());
            if(carousel==null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),"没有查询到当前记录");
            }
            //判断图片是否更新如果更新删除图片
            if(!carousel.getImagePath().equals(carouselOne.getImagePath())){
                FileUploadUtil.deleteFile(environment.getProperty("carouselPath",String.class)+ File.separator+carouselOne.getImagePath());
            }
            carouselMapper.updateCarouselById(carousel);
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"更新成功");
    }

    /**
     * 查询轮播图列表
     * @param carousel
     * @return
     */
    @Override
    public Map<String, Object> queryCarouselList(Carousel carousel) {
        PageInfo<Carousel> pgeCarouselList = null;
        try {
            PageHelper.startPage(carousel.getPageIndex(),carousel.getPageSize(),true);
            List<Carousel> carouselList = carouselMapper.queryCarouselList(carousel);
            pgeCarouselList = new PageInfo<Carousel>(carouselList);
        }catch (Exception e){
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),pgeCarouselList);
    }

    @Override
    @Transactional
    public Map<String, Object> deleteCarouselByIds(List<String> ids) {
        try {
            carouselMapper.deleteCarouselByIds(ids);
            //删除文件
            List<Carousel> carouselList = carouselMapper.queryListByIds(ids);
            for(int i=0;i<carouselList.size();i++){
                FileUploadUtil.deleteFile(environment.getProperty("carouselPath",String.class)+ File.separator+carouselList.get(i).getImagePath());
            }
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
         return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"删除成功");
    }
}
