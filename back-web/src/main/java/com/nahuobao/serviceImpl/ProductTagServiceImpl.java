package com.nahuobao.serviceImpl;

import com.nahuobao.common.ErrorCode;
import com.nahuobao.common.ResultUtil;
import com.nahuobao.entity.ProductTag;
import com.nahuobao.mybatis.ProductTagMapper;
import com.nahuobao.service.ProductTagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by youkun on 2018/3/14.
 * 产品标签
 */
@Service
public class ProductTagServiceImpl implements ProductTagService{

    private Logger logger = Logger.getLogger(ProductTagServiceImpl.class);

    @Autowired
    private ProductTagMapper productTagMapper;

    /**
     * 查询
     * @param productTag
     * @return
     */
    @Override
    public Map<String, Object> queryProductTag(ProductTag productTag) {
        ProductTag query = new ProductTag();
        try {
             query = productTagMapper.queryProductTag(productTag);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),query);
    }

    /**
     * 新增
     * @param productTag
     * @return
     */
    @Override
    public Map<String, Object> insert(ProductTag productTag) {
        try {
            ProductTag query = new ProductTag();
            query.setTagName(productTag.getTagName());
            ProductTag pay = productTagMapper.queryProductTag(query);
            if(pay!=null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),productTag.getTagName()+"已存在!");
            }
            productTagMapper.insert(productTag);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),null);
    }

    /**
     * 更新
     * @param productTag
     * @return
     */
    @Override
    public Map<String, Object> update(ProductTag productTag) {
        try {
            ProductTag query = new ProductTag();
            query.setId(productTag.getId());
            ProductTag pay = productTagMapper.queryProductTag(query);
            if(pay==null){
                return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),"没有查询到当前记录");
            }
            if(!pay.getTagName().equals(productTag.getTagName())){
                ProductTag queryOne = new ProductTag();
                queryOne.setTagName(productTag.getTagName());
                ProductTag payOne = productTagMapper.queryProductTag(queryOne);
                if(payOne!=null){
                    return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10002.getCode(),ErrorCode.ERROR_CODE_10002.getMessage(),productTag.getTagName()+"已存在！");
                }
            }
            productTagMapper.update(productTag);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),"更新成功");
    }

    /**
     * 查询集合
     * @param productTag
     * @return
     */
    @Override
    public Map<String, Object> queryProductTagList(ProductTag productTag) {
        List<ProductTag> query = null;
        try {
            query = productTagMapper.queryProductTagList(productTag);
        }catch (Exception e){
            logger.error(e.getMessage());
            return ResultUtil.resultMap(ErrorCode.ERROR_CODE_10001.getCode(),ErrorCode.ERROR_CODE_10001.getMessage(),e.getMessage());
        }
        return ResultUtil.resultMap(ErrorCode.ERROR_CODE_200.getCode(),ErrorCode.ERROR_CODE_200.getMessage(),query);
    }
}
