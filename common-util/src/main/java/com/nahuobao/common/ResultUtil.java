package com.nahuobao.common;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by youkun on 2018/1/26.
 */
public class ResultUtil {

    public static Map<String,Object>  resultMap(String code,String msg,Object result){
        Map<String,Object> map = new HashMap<String,Object>();
        if(msg!=null){
            map.put("message",msg);
        }else {
            map.put("message","");
        }
        if(code!=null){
            map.put("code",code);
        }else{
            map.put("code","2000");
        }
        if(result!=null){
            map.put("result",result);
        }else {
            map.put("result","");
        }
        return  map;
    }

    public static Map<String,Object>  resultMap(){
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("message","");
        map.put("code","2000");
        map.put("result","");
        return  map;
    }
}
