package com.nahuobao.common;

/**
 * Created by youkun on 2018/3/3.
 * 错误码定义
 */
public enum ErrorCode {

    ERROR_CODE_200("200","成功"),
    ERROR_CODE_10001("10001","系统异常"),
    ERROR_CODE_10002("10002","没有查询到本条记录"),
    ERROR_CODE_10003("10003","数据已存在");

    public String code;

    public String message;

    ErrorCode(String code,String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
